/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.shapeproject;

/**
 *
 * @author MSI
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(3,3);
        System.out.println("Area of rectangle1 (w = " + rectangle1.getW() + " l = " +rectangle1.getL()+") is " + rectangle1.calArea());
        rectangle1.setWL(2,2);
        System.out.println("Area of rectangle1 (w = " + rectangle1.getW() + " l = " +rectangle1.getL()+") is " + rectangle1.calArea());
        rectangle1.setWL(0,0);
        System.out.println("Area of rectangle1 (w = " + rectangle1.getW() + " l = " +rectangle1.getL()+") is " + rectangle1.calArea());
        rectangle1.setWL(5,5);
        System.out.println("Area of rectangle1 (w = " + rectangle1.getW() + " l = " +rectangle1.getL()+") is " + rectangle1.calArea());
    }
}
