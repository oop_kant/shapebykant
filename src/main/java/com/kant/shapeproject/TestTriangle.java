/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.shapeproject;

/**
 *
 * @author MSI
 */
public class TestTriangle {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(5,5);
        System.out.println("Area of Triangle1 (b = " + triangle1.getB() + " h = " +triangle1.getH()+") is " + triangle1.calArea());
        triangle1.setBH(4,4);
        System.out.println("Area of Triangle1 (b = " + triangle1.getB() + " h = " +triangle1.getH()+") is " + triangle1.calArea());
        triangle1.setBH(0,0);
        System.out.println("Area of Triangle1 (b = " + triangle1.getB() + " h = " +triangle1.getH()+") is " + triangle1.calArea());
        triangle1.setBH(3,3);
        System.out.println("Area of Triangle1 (b = " + triangle1.getB() + " h = " +triangle1.getH()+") is " + triangle1.calArea());
    }
}
