/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.shapeproject;

/**
 *
 * @author MSI
 */
public class Rectangle {
    private double w;
    private double l;
    public Rectangle(double w,double l) {
        this.w = w;
        this.l = l;
    }
    public double calArea() {
        return w * l;
    }
    public double getW() {
        return w;
    }
    public double getL() {
        return l;
    }
    public void setWL(double w,double l) {
        if(w<=0){
            System.out.println("Error; wide must more than zero!!!");
            return;
        }
        this.w = w;
        
        if(l<=0){
            System.out.println("Error; long must more than zero!!!");
            return;
        }
        this.l = l;
    }
    
    
    
    
    
}
