/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.shapeproject;

/**
 *
 * @author MSI
 */
public class Triangle {
    private double b;
    private double h;
    public Triangle(double b,double h) {
        this.b = b;
        this.h = h;
    }
    public double calArea() {
        return b * h * 0.5;
    }
    public double getB() {
        return b;
    }
    public double getH() {
        return h;
    }
    public void setBH(double b,double h) {
        if(b<=0){
            System.out.println("Error; base must more than zero!!!");
            return;
        }
        this.b = b;
        
        if(h<=0){
            System.out.println("Error; high must more than zero!!!");
            return;
        }
        this.h = h;
    }
}
